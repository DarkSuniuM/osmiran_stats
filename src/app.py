#!/usr/bin/env python3

from flask import Flask, request, jsonify
from .most_active import *

app = Flask(__name__)

@app.route("/api/most-active", methods=["GET"])
def most_active():
    conn_db = create_connection("assets/db/stats.db")
    results = select_most_active(conn_db, get_first_intervals())

    result_list = []
    for interval in results:
        interval_list = []
        for idx, user in enumerate(interval):
            user_dict = {"index": idx,
                         "username": user[0],
                         "all_changes": user[1],
                         "create_node": user[2],
                         "modify_node": user[3],
                         "delete_node": user[4],
                         "create_way": user[5],
                         "modify_way": user[6],
                         "delete_way": user[7],
                         "create_rel": user[8],
                         "modify_rel": user[9],
                         "delete_rel": user[10]
            }
            interval_list.append(user_dict)
        result_list.append(interval_list)

    res_json={"day_interval": result_list[0],
              "week_interval": result_list[1],
              "month_interval": result_list[2]
    }
    if (conn_db):
        conn_db.close()

    return jsonify(res_json)

if __name__ == '__main__':
    app.run(port="4569", debug=True)

