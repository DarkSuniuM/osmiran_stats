#!/usr/bin/env python3

from pathlib import Path
from datetime import timedelta, timezone
from .to_sqlite import create_connection

from khayyam import JalaliDatetime, TehranTimezone

def get_first_intervals():
    """Get first interval of jalali datetime to UTC
    :param:
    :return: beginning of each interval(day, week, month)
    """

    offset_hour = 4 if JalaliDatetime.now(TehranTimezone()).month <= 6 else 3
    teh_timezone = timezone(timedelta(hours = offset_hour, minutes = 30))
    utc_timezone = timezone(timedelta(0))
    jal_now = JalaliDatetime.now(teh_timezone)

    # Day Beginning
    bige_day = jal_now
    bige_day = bige_day.replace(hour=0, minute=0, second=0, tzinfo=teh_timezone)
    bige_day_str = bige_day.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")

    # Week Beginning
    bige_week = jal_now
    while bige_week.isoweekday() != 1:
        bige_week -= timedelta(days=1)
    bige_week = bige_week.replace(hour=0, minute=0, second=0, tzinfo=teh_timezone)
    bige_week_str = bige_week.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")

    # Month Beginning
    bige_month = jal_now
    bige_month = bige_month.replace(day=1, hour=0, minute=0, second=0, tzinfo=teh_timezone)
    bige_month_str = bige_month.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")

    return {"day": bige_day_str,
            "week": bige_week_str,
            "month": bige_month_str}

def select_most_active(conn, timestamps):
    """query most active users in specific timestamp intervals
    :param conn: Connection object
    :param timestamp: Interval beginning of day, week and month
    :return res: Most active users
    """
    res_list = []
    entry_list = [[timestamps["day"], timestamps["day"], "+1 days"],
                  [timestamps["week"], timestamps["week"], "+7 days"],
                  [timestamps["month"], timestamps["month"], "+1 months"]
    ]

    cur = conn.cursor()
    
    with open("src/sql/select-most-active.sql", "r") as select_active_sql:
        query_sql = select_active_sql.read()
        for entry in entry_list:
            cur.execute(query_sql, entry)
            res_list.append(cur.fetchall())
    return res_list
