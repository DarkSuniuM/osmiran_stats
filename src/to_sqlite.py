#!/usr/bin/env python3

from pathlib import Path
import pprint

from lxml import etree as ET
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

def create_changes_table(conn):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :return:
    """
    try:
        c = conn.cursor()
        with open("src/sql/add-changes-table.sql", "r") as add_table_sql:
            c.execute(add_table_sql.read())
    except Error as e:
        print(e)

def create_files_table(conn):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        cur = conn.cursor()
        with open("src/sql/add-files-table.sql", "r") as add_table_sql:
            cur.execute(add_table_sql.read())
    except Error as e:
        print(e)

def insert_file_to_db(conn, filepath_list):
    """ insert filepath of a changefile to database to avoid
    :param conn: Connection object
    :param filepath: filepath of osc changefile
    :return:
    """
    folder_list = []
    for filepath in filepath_list:
        file_num = int(str(filepath)[-7:-4])
        dir_num = int(str(filepath)[-11:-8])
        ms_dir_num = int(str(filepath)[-15:-12])
        
        file_list = [ms_dir_num, dir_num, file_num, 1]
        folder_list.append(file_list)

    try:
        cursor = conn.cursor()
        with open("src/sql/insert-file.sql", "r") as insert_file_sql:
            cursor.executemany(insert_file_sql.read(), folder_list)
        conn.commit()
        if cursor.rowcount:
            print(f"Folder added to database")
        else:
            print(f"Already file processed.")
        conn.commit()
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to insert multiple records into sqlite table", error)


def select_files(conn):
    """select processed changefiles from database to avoid reprocess
    :param conn: Connection object
    :return:
    """
    cur = conn.cursor()
    
    with open("src/sql/select-files.sql", "r") as select_files_sql:
            cur.execute(select_files_sql.read())

    file_paths = cur.fetchall()
    return [file_path[0] for file_path in file_paths]


def insert_change_entry(conn, list_of_entries):
    """ insert data generated from osc files to database
    :param conn: Connection object
    :param list_of_entries: entries to be inserted
    :return:
    """
    raw_entries = []
    for entry in list_of_entries:
        raw_entry = (entry["type"],
                     entry["operation"],
                     entry["timestamp"],
                     entry["id"],
                     entry["user_id"],
                     entry["file_ref"],
                     entry["username"],
                     entry["version"],
                     entry["changeset"],
                     entry["tag_count"],
                     entry["ref_count"],
                     entry["lat"],
                     entry["lon"]
        )
        raw_entries.append(raw_entry)

    try:
        cursor = conn.cursor()
        with open("src/sql/insert-entry.sql", "r") as insert_entry_sql:
            cursor.executemany(insert_entry_sql.read(), raw_entries)
        conn.commit()
        print(f"Total {cursor.rowcount} records inserted successfully")
        conn.commit()
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to insert multiple records into sqlite table", error)

def gen_entry(osc_path: Path):
    """Generates entry for database
       Parsed by lxml
    :param osc_path: Path object of osc file
    :return: list of generated elements in the file"""
    data_list = []
    
    xml_par = ET.parse(str(osc_path)) # Parse file
    
    # Nodes
    nodes = xml_par.findall(".//node")
    if len(nodes):
        for node in nodes:
            el_dict = {
                    "type": node.tag,
                    "file_ref": str(osc_path)[-15:-4],
                    "operation": node.getparent().tag,
                    "timestamp": node.attrib["timestamp"].replace("T", " ")[:-1],
                    "id": node.attrib["id"],
                    "username": node.attrib["user"],
                    "user_id": node.attrib["uid"],
                    "version": node.attrib["version"],
                    "changeset": node.attrib["changeset"],
                    "tag_count": int(node.xpath("count(./tag)")),
                    "ref_count": None,
                    "lat": node.attrib["lat"],
                    "lon": node.attrib["lon"]
            }
            data_list.append(el_dict)

    # Ways
    ways = xml_par.findall(".//way")
    if len(ways):
        for way in ways:
            el_dict = {
                    "type": way.tag,
                    "file_ref": str(osc_path)[-15:-4],
                    "operation": way.getparent().tag,
                    "timestamp": way.attrib["timestamp"].replace("T", " ")[:-1],
                    "id": way.attrib["id"],
                    "username": way.attrib["user"],
                    "user_id": way.attrib["uid"],
                    "version": way.attrib["version"],
                    "changeset": way.attrib["changeset"],
                    "tag_count": int(way.xpath("count(./tag)")),
                    "ref_count": int(way.xpath("count(./nd)")),
                    "lat": "|".join(way.xpath("./bbox/@*")[::3]),
                    "lon": "|".join(way.xpath("./bbox/@*")[1:3][::-1])
            }
            data_list.append(el_dict)
        
    # Relations
    relations = xml_par.findall(".//relation")
    if len(relations):
        for relation in relations:
            el_dict = {
                    "type": relation.tag,
                    "file_ref": str(osc_path)[-15:-4],
                    "operation": relation.getparent().tag,
                    "timestamp": relation.attrib["timestamp"].replace("T", " ")[:-1],
                    "id": relation.attrib["id"],
                    "username": relation.attrib["user"],
                    "user_id": relation.attrib["uid"],
                    "version": relation.attrib["version"],
                    "changeset": relation.attrib["changeset"],
                    "tag_count": int(relation.xpath("count(./tag)")),
                    "ref_count": int(relation.xpath("count(./member)")),
                    "lat": "|".join(relation.xpath("./bbox/@*")[::3]),
                    "lon": "|".join(relation.xpath("./bbox/@*")[1:3][::-1])
            }
            data_list.append(el_dict)

    return data_list


def main():
    conn_db = create_connection("assets/db/stats.db")
    create_changes_table(conn_db)
    create_files_table(conn_db)
    processed_files = select_files(conn_db)

    osc_dir = Path.cwd() / "assets" / "trimmed-osc"
    
    for folder in osc_dir.glob("004/*"):
        print(f"Processing {str(folder)[-3:]}")
        generated_els = []
        for file in folder.glob("*"):
            if not str(file)[-15:] in processed_files:
                generated_el = gen_entry(file)
                for gen_el in generated_el:
                    generated_els.append(gen_el)
        if len(generated_els):
            insert_file_to_db(conn_db, folder.glob("*"))
            insert_change_entry(conn_db, generated_els)
        print("   ---")

    if (conn_db):
        conn_db.close()
        print("The SQLite connection is closed")

if __name__ == '__main__':
    if Path.cwd().name == "osmiran_stats":
        main()
    else:
        print("Run program from root of repo dir.")