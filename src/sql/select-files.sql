SELECT 
	substr('000'||ms_dir_num,-3)  || "/" ||
	substr('000'||directory_num,-3) || "/" ||
	substr('000'||file_num,-3) || ".osc"
FROM
	files
WHERE
	added_to_db == 1