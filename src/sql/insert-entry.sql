INSERT OR IGNORE INTO "changes" (
  "type",
  "operation",
  "timestamp",
  "id",
  "user_id",
  "file_id",
  "username",
  "version",
  "changeset",
  "tag_count",
  "ref_count",
  "lat",
  "lon"
)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);