CREATE TABLE IF NOT EXISTS "files" (
	"file_num"	INTEGER NOT NULL,
	"directory_num"	INTEGER NOT NULL,
	"ms_dir_num"	INTEGER NOT NULL,
	"added_to_db"	INTEGER NOT NULL DEFAULT 0,
	UNIQUE("file_num", "directory_num", "ms_dir_num", "added_to_db")
);