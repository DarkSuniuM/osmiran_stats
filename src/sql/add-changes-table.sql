CREATE TABLE IF NOT EXISTS "changes" (
  "type"  TEXT NOT NULL,
  "operation"  TEXT NOT NULL,
  "timestamp"  TEXT NOT NULL,
  "id"  INTEGER NOT NULL,
  "user_id"  INTEGER NOT NULL,
  "file_id" TEXT NOT NULL,
  "username"  TEXT NOT NULL,
  "version"  INTEGER NOT NULL,
  "changeset"  INTEGER NOT NULL,
  "tag_count"  INTEGER NOT NULL,
  "ref_count" INTEGER,
  "lat"  TEXT NOT NULL,
  "lon"  TEXT NOT NULL,
  UNIQUE("id", "version")
);