#!/usr/bin/python3
import psycopg2
from configparser import ConfigParser
import pandas as pd


class Postgres:
    def __init__(self):
        self.conn = None

    def config(self, filename='database.ini', section='postgresql'):
        # create a parser
        parser = ConfigParser()
        # read config file
        parser.read(filename)

        # get section, default to postgresql
        db = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db[param[0]] = param[1]
        else:
            raise Exception('Section {0} not found in the {1} file'.format(section, filename))

        return db

    def connect(self):
        """ Connect to the PostgreSQL database server """
        try:
            # read connection parameters
            params = self.config()

            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            self.conn = psycopg2.connect(**params)
            with self.conn.cursor() as cur:
                # execute a statement
                print('PostgreSQL database version:')
                cur.execute('SELECT version()')

                # display the PostgreSQL database server version
                db_version = cur.fetchone()
                print(db_version)

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def close(self):
        # Close connection to database
        if self.conn is not None:
            self.conn.close()
            print('Database connection closed.')

    def exec_query(self, query: str, ids):
        # Execute a query to database
        with self.conn.cursor() as cur:
            # execute a statement
            cur.execute(query, (ids, ))
            return cur.fetchall()


def make_query(type):
    """
    Base query for connection to database
    """
    base_query = """
    SELECT polys_isin, id
    FROM (
    SELECT ST_Within(polys.point, iran.point) as polys_isin, polys.id AS id
    FROM
        (SELECT way AS point
        FROM planet_osm_polygon
        WHERE osm_id=-304938) AS iran,
        (SELECT way AS point, osm_id AS id
        FROM planet_osm_{}
        WHERE osm_id IN %s
        --LIMIT 100
        ) AS polys
    ) AS foo
    --WHERE polys_isin is false;""".format(type)
    return base_query


def remove_outer(source, database, data_type):
    """
    A function to remove datas which has no tag or not in Iran.
    """
    # A map from CSV keywords to database keywords
    type_map = {'node': 'point', 'way': 'line', 'relation': 'polygon'}

    source_by_dt = source[source['type'] == data_type]
    source_by_tuple = tuple(source_by_dt['id'].values.tolist())
    # Generate a database from wethear value is in database or not
    print("Processing datas with {} type...".format(data_type))
    result_dt = pd.DataFrame(database.exec_query(make_query(type_map[data_type]), source_by_tuple), columns=['in_iran', 'id'])

    # If way is selected, we also check polygon table
    if (data_type == 'way'):
        result_dt_2 = pd.DataFrame(database.exec_query(make_query(type_map['relation']), source_by_tuple), columns=['in_iran', 'id'])
        result_dt = pd.concat([result_dt, result_dt_2], ignore_index=True)

    # Filter raw dataframe with checked dataframe
    filtered_dt = source_by_dt[source_by_dt["id"].isin(result_dt["id"])]
    return filtered_dt.reset_index(drop=True)


if __name__ == '__main__':
    # Generate connection to database
    osm_dt = Postgres()
    osm_dt.connect()

    # Select raw CSV
    selected_csv = "2020-01-20.csv"
    stat_list = pd.read_csv("assets/CSV/{}".format(selected_csv))

    # Filter datas from databases
    filtered_node_dt = remove_outer(stat_list, osm_dt, "node")
    filtered_way_dt = remove_outer(stat_list, osm_dt, "way")
    filtered_relation_dt = remove_outer(stat_list, osm_dt, "relation")

    # Close connection to database
    osm_dt.close()

    # Save database to CSV
    filtered_dt = pd.concat([filtered_node_dt, filtered_way_dt, filtered_relation_dt], ignore_index=True)
    filtered_dt.to_csv("assets/Filtered_CSV/{}".format(selected_csv), index=False)
    print("Filtered CSV file saved.")
