#!/usr/bin/python3

#import oauth_cookie_client as oc
import datetime as dt
from bs4 import BeautifulSoup as bs
import requests
import os
from pathlib import Path
import re
import xml.etree.ElementTree as ET

paath = 'assets'

def get_cookie():
    with open('src/cookie/cookie_output_file.txt', 'r') as f:
        coockie_file = f.read()
    return {'cookie' : re.search(r"(.+?);.+", coockie_file).group(1)}

# https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests/16696317#16696317    ///// for download(session)
def download_file(url, filepath, session):
    # NOTE the stream=True parameter below
    with session.get(url, stream=True, headers=get_cookie()) as r:
        r.raise_for_status()
        with open(filepath, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush()
    return filepath

def get_links(session):
    url_internal = 'https://osm-internal.download.geofabrik.de/asia/iran-updates/000/002/'
    response = session.get(url_internal, params={'q': 'requests+language:python'}, headers=get_cookie()).text

    data = []
    for i in bs(response, "html.parser").find_all('li')[1:]:
        link = i.find("a")
        if link.text.endswith(".gz"):
            filename = link.text
            address = url_internal + link["href"]
            data.append({"filename": filename, "address": address})
    return data

def get_dates():
    url = "https://download.geofabrik.de/asia/iran-updates/000/002/"

    rows = bs(requests.get(url).text, "html.parser").find('table').find_all('tr')[3:-1]
    data = []
    for i in rows:
        link = i.find("a")
        if link.text.endswith(".gz"):
            filename = link.text
            date = i.find_all("td")[2].text
            data.append({"filename": (date + filename).replace(" ", "_")})
    return data

def write_csv(dir, date, lines):
    with open(dir + '/' + date, 'w') as f:
        f.write(f"operation,type,id\n")
        for i in lines:
            f.write(f"{i['operation']},{i['type']},{i['id']}\n")

def convert_date(filename):
    rgjsonFile = re.search(r"(.+?)_.+", filename).group(1)
    dateFormat = dt.datetime.strptime(rgjsonFile, '%Y-%m-%d')
    dateDelta = dateFormat - dt.timedelta(days=1)
    return dateDelta.strftime('%Y-%m-%d') + '.csv'

def main():
    # os.system('python3 /home/sosha/GitLab/test/sendfile_osm_oauth_protector/oauth_cookie_client.py -o cookie_output_file.txt -s /home/sosha/GitLab/test/sendfile_osm_oauth_protector/settings.json')

    orgDir = paath + '/OrginalFiles'
    os.makedirs(orgDir, exist_ok=True)

    session = requests.Session()

    files = os.listdir(orgDir)
    for download in get_links(session):
        if download["filename"] in files:
            continue

        adder = download["address"]
        fname = download["filename"]

        # download files here
        print(f"downloading {fname}")
        download_file(adder, orgDir + '/' + fname, session)

    oscDir = paath + '/oscOutput'
    os.makedirs(oscDir, exist_ok=True)

    for gzFile in os.listdir(orgDir):
        if gzFile.endswith(".gz"):
            my_file = Path(oscDir + '/' + gzFile[:-3])
            if not my_file.is_file():
                print(f"Creating osc {my_file}")
                os.system(f"gzip -k -d -c {orgDir}/{gzFile} > {my_file}")

    CSVDir = paath + '/CSV'
    os.makedirs(CSVDir, exist_ok=True)

    for oscFiles in os.listdir(oscDir):
        if oscFiles.endswith(".osc"):
            filetxt = oscFiles[:-4]
            for download in get_dates():
                fname2 = download["filename"]
                if re.match(f".+{filetxt}.+", fname2):
                    date_output = convert_date(fname2)
                    my_file = Path(CSVDir + '/' + date_output)
                    if not my_file.is_file():
                        print(f"Creating CSV {my_file}")

                        root = ET.parse(oscDir + '/' + oscFiles).getroot()

                        lines = []
                        for operation in ["create", "modify", "delete"]:
                            for typei in ["node", "way", "relation"]:
                                for x in root.findall(operation):
                                    for item in x.findall(typei):
                                        lines.append({"operation": operation, "type": typei, "id": item.attrib["id"]})
                        write_csv(CSVDir, date_output, lines)


if __name__ == "__main__":
    main()
