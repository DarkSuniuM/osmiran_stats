FROM python:3.8-slim-buster

WORKDIR /usr/src/osmiran_stats

COPY ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt && \
    pip install gunicorn
COPY . .

EXPOSE 8080

CMD [ "pipenv", "shell"]
CMD [ "pipenv", "run", "./main.py" ]
