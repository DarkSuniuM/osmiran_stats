#!/usr/bin/env python3

import schedule
from pathlib import Path
import subprocess
import time

def down_and_store(): # To be run every 6 hours
    gather_retc = subprocess.call(["python3", "src/gather.py"])
    while not gather_retc:
        gather_retc = subprocess.call(["pythno3", "src/gather.py"])
    
    to_db_retc = subprocess.call(["python3", "src/to_sqlite.py"])
    while not to_db_retc:
        to_db_retc = subprocess.call(["python3", "src/to_sqlite.py"])

# def api():
#     flask_api = subprocess.call(["python3", "src/app.py"])
#     return schedule.CancelJob


schedule.every(6).hours.do(down_and_store)
# schedule.every(1).second.do(api)

down_and_store()

if __name__ == "__main__":
    if Path.cwd().name != "osmiran_stats":
            raise("Run program from root of repo dir.")
    while True:
        schedule.run_pending()
        time.sleep(1)